# TS3 Scripting Template

This project is designed is a [SDK-style](https://learn.microsoft.com/en-us/dotnet/core/project-sdk/overview) template that targets .NET Framework 2.0 so you can get straight into creating scripting mods for The Sims 3 while taking advantage of the latest language features. This includes a basic hello world-like example from [Sims Wiki](http://simswiki.info/wiki.php?title=Tutorial:Sims_3_Pure_Scripting_Modding).

## Requirements

* .NET 6 or later
* Visual Studio
  * .NET desktop workload
* TS3: Patch 1.69 (Origin)

## License

I project is licensed under the GPL v2.0 license - see the [LICENSE](LICENSE) file for details.

## Disclaimer

This project is not in any way affiliated with Electronic Arts or EA Maxis.
